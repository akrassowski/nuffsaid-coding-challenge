
"""container for search details"""
import csv

class Detail:
    """catch-all container for search-support details"""
    COMMON_WORDS = ['SCHOOL'] # skip these when scoring
    NAME_SKIP_PHRASES = ['CHARTER OAK']  # interferes with CHARTER keyword in name

    def __init__(self, school_data_filename, states_filename):
        self.school_info_by_id = {}
        self.id_list_by_state_code = {}
        self.id_list_by_word_in_city = {}
        self.id_list_by_city = {}
        self.id_list_by_word_in_name = {}
        self.state_code_by_state_name = {}
        self.__school_id = 0

        # initialize search helpers
        self.load_school_data(filename=school_data_filename, encoding='cp1252')
        self.load_states_map(filename=states_filename)

    def __str__(self):
        s = ""
        s += f'{len(self.school_info_by_id)=}\n'
        s += f'{len(self.id_list_by_state_code)=}\n'
        s += f'{len(self.id_list_by_word_in_city)=}\n'
        s += f'{len(self.id_list_by_city)=}\n'
        s += f'{len(self.id_list_by_word_in_name)=}\n'
        s += f'{len(self.state_code_by_state_name)=}\n'
        return s

    def append(self, dic, key):
        """wrapper instead of defaultdict(list)"""
        if key in dic:
            dic[key].append(self.__school_id)
        else:
            dic[key] = [self.__school_id]

    def add_school_info(self, info):
        """stash the info for current school"""
        self.school_info_by_id[self.__school_id] = info

    def next_id(self):
        """move on to next school id"""
        self.__school_id += 1

    def get_school_info_by_id(self, ref):
        """hides storage impl"""
        return self.school_info_by_id[ref]

    def load_states_map(self, filename, encoding='utf-8'):
        """prepare mapping from state name text to state_code to support searching"""
        try:
            with open(filename, 'r', encoding=encoding) as csvfile:
                reader = csv.reader(csvfile, delimiter=',')
                for line in reader:
                    state, two_letter = line
                    self.state_code_by_state_name[state.strip().upper()] = two_letter.strip()
        except UnicodeDecodeError as ex:
            print(f"line:{reader.line_num} {ex}")

    def load_school_data(self, filename, encoding):
        """prepare search-helper data structures"""
        try:
            with open(filename, 'r', encoding=encoding) as csvfile:
                reader = csv.reader(csvfile, delimiter=',')
                next(reader) # discard header
                for line in reader:
                    self.parse_line(line)
        except UnicodeDecodeError as ex:
            print(f"line:{reader.line_num} {ex}")

    def parse_line(self, line):
        """process a line into various dictionaries to help score search text"""
        # NCES, LEA, agency, school_name, city, state_code, lati, longi, mlocale, elocale, status
        school_name, city, state_code = line[3], line[4], line[5]

        self.add_school_info((school_name, city, state_code))

        # prepare single-word city map
        self.append(self.id_list_by_city, city)

        # prepare multy-word city map
        city_words = city.split()
        if len(city_words) > 1:
            for word in city.split():
                self.append(self.id_list_by_word_in_city, word)

        # state abbreviation map
        self.append(self.id_list_by_state_code, state_code)

        # remove certain phrases in a school's name before mapping
        for phrase in Detail.NAME_SKIP_PHRASES:
            school_name = school_name.replace(phrase, '')

        words = [w for w in school_name.split() if w not in Detail.COMMON_WORDS]
        for word in words:
            self.append(self.id_list_by_word_in_name, word)

        self.next_id()

if __name__ == "__main__":
    dtl = Detail('school_data.csv', 'region_states.csv')
    print(dtl)

# end
