#!/bin/env python'''

'''
Problem Statement:
Create a file called count_schools.py, and write a method called print_counts.
Please implement all of these features using pure python.

You may use the following libraries: csv, time, itertools
No other libraries may be used.

Example output (this is just a sample, the lists have been truncated and counts aren't
correct)
>>> count_schools.print_counts()
Total Schools: 10000
Schools by State:
CO: 1000
DC: 200
AK: 600
DE: 300
AL: 1500
AR: 1100
...
Schools by Metro-centric locale:
1: 3000
3: 2000
2: 5000
5: 300
...
City with most schools: CHICAGO (50 schools)
Unique cities with at least one school: 1000
'''
import csv

class Details:
    school_count = 0
    school_count_by_state_dict = {}
    school_count_by_mlocale_dict = {}
    school_count_by_city_dict = {}


def parse_line(details, line):
    def increment(key, dic):
        if key in dic:
            dic[key] += 1
        else:
            dic[key] = 1

    (NCES_id, LEA_id, agency,
     school_name, city, state,
     latitude, longitude, mlocale, ulocale, status) = line

    details.school_count += 1
    increment(state, details.school_count_by_state_dict)
    increment(mlocale, details.school_count_by_mlocale_dict)
    increment(city, details.school_count_by_city_dict)

def get_total_schools(details):
    return f"Total Schools: {details.school_count}"

def get_schools_by_state(details):
    answer = "Schools by State:\n"
    for state, count in details.school_count_by_state_dict.items():
        answer += f"{state:2}: {count}\n"
    return answer

def get_schools_by_mlocale(details):
    answer = "Schools by Metro-centric locale:\n"
    for mlocale, count in details.school_count_by_mlocale_dict.items():
        answer += f"{mlocale}: {count}\n"
    return answer

def get_city_with_most_schools(details):
    most_cities = []
    most = 0
    for city, count in details.school_count_by_city_dict.items():
        if count == most:
            most_cities.append(city)
        elif count > most:
            most_cities = [city]
            most = count
    # handle same count
    if len(most_cities) > 1:
        answer = f"Cities with most schools: {', '.join(most_cities)}"
    else:
        answer = f"City with most schools: {most_cities[0]}"
    answer += f" ({most} schools)"
    return answer

def get_unique_cities(details):
    at_least_one_school_count = 0
    for count in details.school_count_by_city_dict.values():
        if count >= 1:
            at_least_one_school_count += 1
    return f"Unique cities with at least one school: {at_least_one_school_count}"

def load_school_data(details, filename, encoding):
    try:
        with open(filename, 'r', encoding=encoding) as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            next(reader) # discard header
            for line in reader:
                parse_line(details, line)
    except UnicodeDecodeError as ex:
        print(f"line:{reader.line_num} {ex}")

def print_counts():
    """entry method"""
    details = Details()
    load_school_data(details, filename='school_data.csv', encoding='cp1252')
    print(get_total_schools(details), '\n')
    print(get_schools_by_state(details), '\n')
    print(get_schools_by_mlocale(details), '\n')
    print(get_city_with_most_schools(details), '\n')
    print(get_unique_cities(details), '\n')

if __name__ == "__main__":
    print_counts()
