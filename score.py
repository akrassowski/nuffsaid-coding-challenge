"""helpers to score school searches"""

class Score:
    """used to measure quality of a text search"""

    #TODO adjust weights for better balance across larger dataset; primes help troubleshooting
    SCORE_WORD = 15
    SCORE_CITY = 17
    SCORE_CITY_EXACT = 49
    SCORE_CITY_USED = SCORE_CITY / 2
    SCORE_CITY_EXACT_USED = SCORE_CITY_EXACT / 2

    SCORE_STATE = 59 # 2
    SCORE_NAME = 13
    SCORE_NAME_EXACT = 277
    SCORE_NAME_KEYWORD = 377
    NAME_KEYWORDS = ['CHARTER']

    def __init__(self, detail):
        self.detail = detail
        self.clear()

    def clear(self):
        """reset scoring activity"""
        self.score_by_id = {}
        self.id_list_used_in_name = {}

    def plus(self, key, val):
        """add value to score or insert new score for this key"""
        if key in self.score_by_id:
            self.score_by_id[key] += val
        else:
            self.score_by_id[key] = val

    def get_nlargest(self, num):
        """return max num-length array of ids with largest scores"""
        winners = []
        while num > 0:
            best_key = max(self.score_by_id, key=self.score_by_id.get)
            val = self.score_by_id[best_key]
            if val == 0:
                break
            #print('best score', val)
            winners.append(best_key)
            self.score_by_id[best_key] = 0
            num -= 1
        return winners

    def score_whole_name(self, words):
        """points for whole name match or all words in name match in any order"""

        # start with ids for last word as this may be smallest list
        refs = set(self.detail.id_list_by_word_in_name.get(words[-1], []))
        for word in reversed(words[:-1]):
            ids = self.detail.id_list_by_word_in_name.get(word, [])
            # keep only ids common to each new set
            refs = refs.intersection(set(ids))
            if not refs:
                break    ## early exit as soon as 0 words in common

        for ref in refs or []:
            self.plus(ref, Score.SCORE_NAME_EXACT)

    def score_word_in_name(self, word):
        """big score for keyword, else score a word in the school name"""
        ids = None
        if word in Score.NAME_KEYWORDS:
            ids = self.detail.id_list_by_word_in_name.get(word)
            value = Score.SCORE_NAME_KEYWORD
        if ids is None:
            ids = self.detail.id_list_by_word_in_name.get(word, [])
            value = Score.SCORE_WORD

        if ids is not None:
            self.id_list_used_in_name[word] = ids

        for ref in ids:
            self.plus(ref, value)

    def score_state(self, word):
        """score state abbreviation or any 2-letter word that matches state"""
        # TODO: multi-word states need more work
        ids = []
        if len(word) > 2:
            two_letter = self.detail.state_code_by_state_name.get(word)
            if two_letter:
                ids = self.detail.id_list_by_state_code.get(two_letter, [])
        elif len(word) == 2:
            ids = self.detail.id_list_by_state_code.get(word, [])
        for ref in ids:
            self.plus(ref, Score.SCORE_STATE)

    def score_city(self, word):
        """score word in city for both exact and partial match, skip if used in name"""

        used_ids = None  # fetch used_ids only if needed and only once

        # score partial
        ids = self.detail.id_list_by_word_in_city.get(word)
        if ids:
            used_ids = self.id_list_used_in_name.get(word)
            for ref in ids or []:
                value = Score.SCORE_CITY_USED if ref in used_ids else Score.SCORE_CITY
                self.plus(ref, value)

        # and score exact
        ids = self.detail.id_list_by_city.get(word)
        if ids:
            if used_ids is None:
                used_ids = self.id_list_used_in_name.get(word)
            for ref in ids or []:
                value = Score.SCORE_CITY_EXACT_USED if ref in used_ids else Score.SCORE_CITY_EXACT
                self.plus(ref, value)
# end
