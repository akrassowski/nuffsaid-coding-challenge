
'''
Problem Statement:
This feature should search over school name, city name, and state name.
The top 3 matching results should be returned (see below for examples).
'''
import time

# TODO - typically check for Python version 3.6 or better but cannot import sys
# Tested with 3.8.6
# import sys
#if sys.version_info.major != 3 or sys.version_info.minor != 8:
#    print("Expecting python version 3.8, got {} exiting".format(sys.version_info))
#    sys.exit(-1)

from detail import Detail
from score import Score

global detail
detail = Detail('school_data.csv', 'region_states.csv')

def search_schools(search_string):
    """main call, expects details to be setup"""
    #TODO load each invocation to comply with solution constraints
    global detail

    start_time = time.time()                  # <<<<<<<<<<<<<<<<<<< start the clock
    score = Score(detail)
    uc_words = [w for w in search_string.upper().split() if w not in Detail.COMMON_WORDS]

    score.score_whole_name(uc_words)
    for word in uc_words:
        score.score_word_in_name(word)
        score.score_state(word)
        score.score_city(word)

    winners = score.get_nlargest(3)

    secs = time.time() - start_time # <<<<<<<<<<<<<<<<<<<< stop the clock

    print_search_results(search_string, secs, winners)

def print_search_results(search_string, secs, winners):
    """show the results for a search"""
    print(f'Results for "{search_string}" (search took: {secs:.5f}s)')
    for num, ref in enumerate(winners):
        name, city, state = detail.get_school_info_by_id(ref)
        print(f'{num+1}. {name}')
        print(f'{city}, {state}')
    print()


if __name__ == "__main__":
    for search_text in [
            'elementary school highland park',
            'jefferson belleville',
            'riverside school 44',
            'granada charter school',
            'foley high alabama',
            'KUSKOKWIM',
    ]:
        search_schools(search_text)
